# Automated Colours #

### What is this repository for? ###

* This is an example of what you can do by chaining front end tools together.
* Version 1.0.0

### How do I get set up? ###

* Download the repo
* Install node if you haven't already http://bfy.tw/AhIO
* Go to the path in terminal/commandline
* Run 'npm install'
* Edit ./sass/configuration/_colours.scss (optional)
* Run 'gulp setup' in the root folder
* Run 'gulp go'

### Stuck, got an idea or just want to chat shop? ###

* Find me on twitter @ jmdrawneek