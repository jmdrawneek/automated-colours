
// Dependencies.
var util = require('util'),
    async = require('async'),
    _ = require('underscore'),
    stringHelper = require('../resources/js-helpers/string-processors'),
    settings = require('../gulp_settings');

// Abstracted functions.
var fn = require('../resources/js-processors/evaluates');


////// TEMP //////
var links = settings.location.testSite;
var referenceUrl = settings.location.testSiteReference;

// TODO: Move this to somewhere sensible.

var CssProperties = [
  'background-color',
  'color',
  'border-color'
];


var CssPropertiesObj = {};

// Create a new object and convert the css properties to strings compatible with use as keys.

for (var key in CssProperties) {
  var new_key = stringHelper.camelCase(CssProperties[key]);
  CssPropertiesObj[new_key] = CssProperties[key];
}

var total = links.length;


casper.start(referenceUrl[0].url, function () {
  casper.on('remote.message', function(message) {
    this.echo(message);
  });


  fn.propertyCollector(CssPropertiesObj, 'reference-palette', referenceUrl[0].url);

  async.eachOfSeries(links, function (link, key, callback) {

    casper.echo('Total left: ' + (total - key), 'SKIP');
    // Opens defined urls.
    casper.thenOpen(link.url)
        .then(function () {
          fn.propertyCollector(CssPropertiesObj, 'target-palette', link.url);
        })
        .then(function () {
          testColours();
        });

  });

}).run(function() {
  this.echo('Resources collected.');
  this.exit();
});

function testColours() {

  casper.test.begin('Testing colours', function (test) {

    var reference = require('../results/resources/reference-palette.json'),
        targetCollection = require('../results/resources/target-palette.json'),
        reference_colours = fn.returnOnlyProperties(reference, ['background-color'], 'properties', 'multi'),
        failed = [];

    for (var item in targetCollection) {

        var checks = [],
        properties_needed = [
          'background-color',
          'color',
          'border-color'
        ];

        // Convert object into collection.
        targetCollection[item].colours = fn.returnOnlyProperties(targetCollection[item], properties_needed, 'properties', 'single');
        targetCollection[item].failedColours = [],
        targetCollection[item].failed = [];

        // Check each item in the target object.
        for (var property in targetCollection[item].colours) {

          (function checkAgainstColours() {

            var noToCheck = reference_colours.length,
                i = 0;
            for (var colour in reference_colours) {
              i++;

              if (String(targetCollection[item].colours[property]) == String(reference_colours[colour])) {
                checks.push(true);
              }
              else {
                checks.push(false);
              }
              if (i === noToCheck) {
                return checks;
              }
            }

            return checks;
          })();

          // Add failed colours to new object in item.
          if (!fn.checkForTrue(checks)) {
            targetCollection[item].failedColours.push(targetCollection[item].colours[property]);
          }

          // Add true/false
          if (fn.checkForTrue(checks)) {
            targetCollection[item].failed.push(false);
          }
          else {
            targetCollection[item].failed.push(true);
          }
        }
    }

    var failed_items = _.filter(targetCollection, function (item) {
      for (var checking in item.failed) {
        if(item.failed[checking] === true) {
          return item.failed;
        }
      }
    });

    var wrapped_items = [];

    for (item in failed_items) {
      wrapped_items.push({
        item: failed_items[item]
      });
    }

    fs.write(
        './gulp/results/failed-colours.json',

        JSON.stringify(wrapped_items, null, 10),

        function (err) {
          if (err) {
            console.error('Broken');
          }
        }
    );

    if (failed_items.length > 0) {
      test.assert(false, 'Found colours not in the palette');
    }
  });
}
