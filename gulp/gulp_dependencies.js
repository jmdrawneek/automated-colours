
const need = {};
// General gulp modules.
need.gulp = require('gulp');
// Only use sass/etc files with changes in.
need.changed = require('gulp-changed');
// Run bash commands from gulp.
need.shell = require('gulp-shell');
// Conditional statements in gup
need.gulpif = require('gulp-if');

need.rename = require('gulp-rename');

// Browser Sync
need.bs = require("browser-sync");

// File system module.
//need.fs = require('fs');
need.fs = require('fs-extra');

// Add colours to the console!
need.colors = require('colors');

need.async = require('async');

need.path = require('path');
// Allows simple git commands.
need.simpleGit = require('simple-git')();


// Needed to compile sass to css.
need.sass = require('gulp-sass');

// Adds sass source maps.
need.sourcemaps = require('gulp-sourcemaps');
need.sassGlob = require('gulp-sass-glob');

need.sourcepaths = [];


// This is used by iconify.
need.runSequence = require('run-sequence');


//
//need.livereload = require('gulp-livereload');
need.kss = require('kss');
need.gulpkss = require('gulp-kss');


need.handlebars = require('gulp-compile-handlebars');


need.rename = require('gulp-rename');

module.exports = need;