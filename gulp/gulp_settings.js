// Gulp variables

// Location based variables.
const location = [];

location['testSite'] = [{"url":"http://localhost:3030/output-styles/index.html","status":200}];
location['testSiteReference'] = [{"url":"http://localhost:3000/section-1.html","status":200}];

// Stylesheet sources and destinations.
location['stylessrc'] = 'sass';
location['csssource'] = '*.styles.scss';

location['reports'] = 'reports';

// All sass that's generated should be added here to the master sass file will pick it up.
location['gensass'] = [];

location['cssdest'] = 'output-styles';
location['cssoutput'] = 'master.css';

location['styleguide'] = 'styleguide';

const watch = {
  sass: [location['stylessrc'] + '/**/*.scss', location['stylessrc'] + '/*.scss'],
  twig: ['template/**/*.html.twig'],
  js: [location['js'] + '/*.js']
};


// Export the settings to use on other gulp files.
exports.location = location;
exports.watch = watch;