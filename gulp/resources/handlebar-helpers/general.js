var stringHelpers = require('../js-helpers/string-processors');

module.exports = {
  stringify: function (str) {
    return JSON.stringify(str);
  },
  neaten: function (str) {
    return str.substr(0, str.lastIndexOf('.')) || str;
  },
  debug: function (optionalValue) {
    console.log("Current Context");
    console.log("====================");
    console.log(this);

    if (optionalValue) {
      console.log("Value");
      console.log("====================");
      console.log(optionalValue);
    }
  },
  stringToLower: function (str) {
    return stringHelpers.lowerCase(str);
  },
  reverseCamelCaseHB: function (str) {
    return stringHelpers.reverseCamelCase(str);
  },
  removeNonWordHB: function (str) {
    return stringHelpers.removeNonWord(str);
  },
  upperCaseHB: function (str) {
    return stringHelpers.upperCase(str);
  },
  properCaseHB: function (str) {
    return stringHelpers.properCase(str);
  },
  checkForColoursHB: function (str) {
    return stringHelpers.checkForColours(str);
  },
  if_eq: function(a, b) {
    if (a === b) { // Or === depending on your needs
      return true;
    }
    else {
      return false;
    }
  }

};

// properCase

//  <!--<span class="colour-sample" style="background-color: {{ this }};"></span>{{ this }}-->
