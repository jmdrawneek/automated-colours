module.exports = {

  /**
   * Replaces all accented chars with regular ones
   */
  replaceAccents: function (str) {
    // verifies if the String has accents and replace them
    if (str.search(/[\xC0-\xFF]/g) > -1) {
      str = str
          .replace(/[\xC0-\xC5]/g, "A")
          .replace(/[\xC6]/g, "AE")
          .replace(/[\xC7]/g, "C")
          .replace(/[\xC8-\xCB]/g, "E")
          .replace(/[\xCC-\xCF]/g, "I")
          .replace(/[\xD0]/g, "D")
          .replace(/[\xD1]/g, "N")
          .replace(/[\xD2-\xD6\xD8]/g, "O")
          .replace(/[\xD9-\xDC]/g, "U")
          .replace(/[\xDD]/g, "Y")
          .replace(/[\xDE]/g, "P")
          .replace(/[\xE0-\xE5]/g, "a")
          .replace(/[\xE6]/g, "ae")
          .replace(/[\xE7]/g, "c")
          .replace(/[\xE8-\xEB]/g, "e")
          .replace(/[\xEC-\xEF]/g, "i")
          .replace(/[\xF1]/g, "n")
          .replace(/[\xF2-\xF6\xF8]/g, "o")
          .replace(/[\xF9-\xFC]/g, "u")
          .replace(/[\xFE]/g, "p")
          .replace(/[\xFD\xFF]/g, "y");
    }

    return str;
  },

  /**
   * "Safer" String.toLowerCase()
   */
  lowerCase: function (str) {
    return str.toLowerCase();
  },

  /**
   * "Safer" String.toUpperCase()
   */
  upperCase: function (str) {
    return str.toUpperCase();
  },

  /**
   * UPPERCASE first char of each word.
   */
  properCase: function (str) {
    return str.lowerCase().replace(/^\w|\s\w/g, this.upperCase);
  },

  /**
   * Remove non-word chars.
   */
  removeNonWord: function (str) {
    return str.replace(/[^0-9a-zA-Z\xC0-\xFF \-]/g, '');
  },

  /**
   * Convert string to camelCase text.
   */
  camelCase: function (str) {
    str = this.replaceAccents(str);
    str = this.removeNonWord(str)
        .replace(/\-/g, ' ') //convert all hyphens to spaces
        .replace(/\s[a-z]/g, this.upperCase) //convert first char of each word to UPPERCASE
        .replace(/\s+/g, '') //remove spaces
        .replace(/^[A-Z]/g, this.lowerCase); //convert first char to lowercase
    return str;
  },

  /**
   *Reverse camelCase text.
   */
  reverseCamelCase: function (str) {
    str = str.toString();
    return str.replace(/([A-Z])/g, '-$1').toLowerCase();
  },
  checkForColours: function (item) {

    if (typeof item === 'string') {
      //item = item.replaceAll("^\"|\"$", "");
      var rgba = item.search(/rgba \(/g);
      var rgb = item.search(/rgb\(/g);

      if (rgba > -1 || rgb > -1) {
        //console.log(JSON.stringify(item));

        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }

  }
};
