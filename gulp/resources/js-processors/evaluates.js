var _ = require('underscore'),
    stringHelper = require('../js-helpers/string-processors');

module.exports = {
  propertyCollector: function (CssPropertiesObj, type, link) {
    var cleanEmptyObjects = this.cleanEmptyObjects;

    casper.then(function () {

      var itemList = this.evaluate(function (CssPropertiesObj, type, link, cleanEmptyObjects) {
        var page = this;

        console.log('----- Evaluating ' + link + ' -----');

        var selector = type === 'reference-palette' ? '.kss-modifier__example *' : '*',
         nodes = document.querySelectorAll(selector);

        var finalList = [].map.call(nodes, function (node) {

          var get_class = node.getAttribute('class'),
           get_element_type = node.tagName,
           node_item = {};

          var unwanted_elements = [
            'LINK',
            'TITLE',
            'SCRIPT',
            'HTML',
            'HEAD',
            'META'
          ];

          // Remove the unwanted elements from the reference object.
          if (type !== 'reference-palette' &&
              unwanted_elements.indexOf(get_element_type) < 0 ||
              type === 'reference-palette' &&
              get_class !== '[modifier class]' && get_class !== '' && get_class !== 'kss-modifier__example-footer') {

            node_item['properties'] = {};

            for (var key in CssPropertiesObj) {
              node_item['properties'][key] = page.getComputedStyle(node, null).getPropertyValue(CssPropertiesObj[key]);
            }

            // Add identification information.
            if (type !== 'reference-palette') {
              node_item['properties']['classes'] = node.getAttribute('class');
              node_item['properties']['element'] = node.tagName;
              node_item['properties']['url'] = link;

              // Add random number to use as reference later.
              var hash = Math.round((Math.random() * 1)) * (new Date()).valueOf().toString();
              node.setAttribute('data-hash', String(hash));
              node_item['hash_id'] = node.getAttribute('data-hash');
            }

          }
          return node_item;

        });

        return cleanEmptyObjects(finalList);

      }, CssPropertiesObj, type, link, cleanEmptyObjects);


      fs.write(
          './gulp/results/resources/' + type + '.json',

          JSON.stringify(itemList, null, 10),

          function (err) {
            if (err) {
              console.error('Broken');
            }
          }
      );
    });

  },
  returnJustValues: function (object) {
    var single_colours = _.uniq(_.flatten(_.map(object, _.values)));
    return single_colours;
  },
  returnOnlyProperties: function (object, properties, optionalMapping, multi) {
    var list = [];
    for (var property in properties) {
      var property_clean = stringHelper.camelCase(properties[property]);
      if (optionalMapping) {
        if (multi === 'single') {
          for (var keyTest in object[optionalMapping]) {
            if (keyTest === property_clean) {
              list.push(object[optionalMapping][keyTest]);
            }
          }
        }
        if (multi === 'multi') {
          // Each key value pair.
          for (var item in object) {
            for (var keyOne in object[item][optionalMapping]) {
              if (keyOne === property_clean) {
                list.push(object[item][optionalMapping][keyOne]);
              }
            }
          }
        }
      }
      else {
        // Each key value pair.
        for (var keyTwo in object) {

          if (keyTwo === property_clean) {
            list.push(object[keyTwo]);
          }
        }
      }
    }

    return list;
  },
  cleanEmptyObjects: function (objects) {
    var cleaned = objects.filter(function (item) {
      return Object.keys(item).length > 0;
    });
    return cleaned;
  },
  checkForTrue: function (array) {
    var checkFails = _.filter(array, function (check) {
      return check === true;
    });
    return checkFails.length > 0;
  }
};
