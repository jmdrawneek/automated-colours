/**
 * Registers the "sitename" Handlebars helper.
 *
 * @param {object} handlebars The global Handlebars object used by kss-node's kssHandlebarsGenerator.
 */

var sitesettings = require('../../gulp_settings');

var iconno = 49;

module.exports.register = function (handlebars) {

    
    // Pass client name to kss node template.
    handlebars.registerHelper('sitename', function () {
        return new handlebars.SafeString(sitesettings.sitename);
    });

    handlebars.registerHelper('iconno', function () {
        iconno++;
        console.log(arguments);
        return new handlebars.SafeString(iconno);
    });

    Template.registerHelper('log', function(something) {
        console.log(something);
    });

};
