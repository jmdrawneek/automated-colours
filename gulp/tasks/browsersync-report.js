var browsersyncreport = function (task, gulp, sitesettings, need) {
  'use strict';

  // Browser Sync setup

  gulp.task('browsersync-report', ['builder'], function () {

    const bsConfig = {
      server: {
        baseDir: "./",
        index: "reports/index.html",
        files: ['output-styles/*.css', 'reports/*.html', 'reports/*.css'],
        logLevel: "debug",
        routes: {
          "/": "./reports"
        },
        ghostMode: {
          clicks: false,
          forms: false,
          scroll: false
        },
        reloadThrottle: 1
      },
      port: 3020,
      ui: {
        port: 3023
      }
    };

    need.bs.init(bsConfig);

    need.bs = require('browser-sync').create(); // create a browser sync instance.

    function debounce(fn, delay) {
      var timer = null;
      return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
          fn.apply(context, args);
        }, delay);
      };
    }


    gulp.watch([sitesettings.location['reports'] + '/**/*.html'], {
      debounceDelay: 2000
    }).on('change', debounce(
        need.bs.reload
    ), 1000);


  });

  };

module.exports = browsersyncreport;