var browsersync = function (task, gulp, sitesettings, need) {
  'use strict';

  // Browser Sync setup

  gulp.task('browsersync-test-area', function () {

    const bsConfig = {
      server: {
        baseDir: "./",
        index: "output-styles/index.html",
        files: ['output-styles/*.css', 'styleguide/*.html'],
        logLevel: "debug",
        routes: {
          "/": "./output-styles"
        },
        ghostMode: {
          clicks: true,
          forms: true,
          scroll: false
        },
        reloadThrottle: 1
      },
      port: 3030,
      ui: {
        port: 3033
      }
    };

    need.bs.init(bsConfig);

    need.bs = require('browser-sync').create(); // create a browser sync instance.

  });

  };

module.exports = browsersync;