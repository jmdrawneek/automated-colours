var browsersync = function (task, gulp, sitesettings, need, taskObj) {
  'use strict';

  // Browser Sync setup

  gulp.task('browsersync', ['casper-factory-tests'], function () {
    
    // Get details from the master task object.
    var taskServerDetails = taskObj[global.process.argv[2]].taskData;


    const bsConfig = {
      server: {
        logLevel: "debug",
        ghostMode: {
          clicks: true,
          forms: true,
          scroll: false
        },
        reloadThrottle: 1
      },
      port: 3000,
      ui: {
        port: 3001
      }
    };

    // Check if we want a proxied server or static html server.
    if (taskServerDetails.proxy) {
      bsConfig.server.proxy = taskServerDetails.proxy;
    }
    else if (taskServerDetails.index) {
      bsConfig.server.baseDir =  "./";
      bsConfig.server.index = taskServerDetails.index;
      bsConfig.server.files = taskServerDetails.serverFiles;
      bsConfig.server.routes = taskServerDetails.routes;
    }

    console.log(taskServerDetails.serverFiles);

    need.bs.init(bsConfig);

    function debounce(fn, delay) {
      var timer = null;
      return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
          fn.apply(context, args);
        }, delay);
      };
    }

    need.bs = require('browser-sync').create(); // create a browser sync instance.


    gulp.watch([sitesettings.location['stylessrc'] + '/**/*.scss', sitesettings.location['stylessrc'] + '/*.scss', 'template/**/*.html.twig'], {
      debounceDelay: 2000
    }).on('change', debounce(
        need.bs.reload
    ), 1000);
  });

  };

module.exports = browsersync;