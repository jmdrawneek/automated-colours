var builder = function (task, gulp, sitesettings, need, tasksooj) {
  'use strict';

  const fs = require('fs-extra');

  gulp.task('builder', ['kssgen'], function () {

    // Create array with test file names.
    const templates = [],
     helperObject = require('../resources/handlebar-helpers/general'),
     stringHelper = require('../resources/js-helpers/string-processors'),
     results = need.path.join(__dirname, '../results'),
        reports = need.path.join(__dirname, '../../reports'),
     rootTemplates = need.path.join(__dirname, '../builders/templates'),
     reportList = [];

    fs.readdir(results, function (err, items) {
      for (var i = 0; i < items.length; i++) {
        console.log(String(items[i]));
        if (items[i].endsWith('.json')) {
          var newname = items[i].substr(0, items[i].lastIndexOf(".")) + ".html";

          reportList.push(newname);

          var data = require(results + '/' + items[i]);

          for (var item in data) {

            for (var prop in data[item]['item']['properties']) {
              switch (prop) {
                case 'element':
                   var rawTag = stringHelper.lowerCase(String(data[item]['item']['properties']['element']));
                   data[item]['item']['properties']['element'] = '&lt;' + rawTag + '&gt;';
                  break;
              }

              if(stringHelper.checkForColours(data[item]['item']['properties'][prop])) {
                var rawColour = data[item]['item']['properties'][prop];
                data[item]['item']['properties'][prop] = '<span class="colour-sample" style="background-color:' + rawColour + '"></span>' + rawColour;
              }
            }
          }

          templates.push({
            template: rootTemplates + '/page.handlebars',
            html: newname
          });
        }
      }

      var templateVars = {
            dev: false,
            url: 'test',
            data: data,
            reports: reportList
          },
          options = {
            ignorePartials: true, // Ignores the unknown partials.
            batch: ['./gulp/builders/templates/partials'],
            helpers: helperObject,
            noEscape: true
          };

      return templates.forEach(function (newname) {

        return gulp.src(newname.template)
            .pipe(need.handlebars(templateVars, options))
            .pipe(need.rename(newname.html))
            .pipe(gulp.dest('./reports'));
      })

    });

    function singlePageGen(template, reportList) {

      var templateVars = {
            dev: false,
            url: 'test',
            reports: reportList
          },
          options = {
            ignorePartials: true, // Ignores the unknown partials.
            batch: ['./gulp/builders/templates/partials'],
            helpers: helperObject,
            noEscape: true
          };

      return gulp.src(template)
          .pipe(need.handlebars(templateVars, options))
          .pipe(need.rename('index.html'))
          .pipe(gulp.dest('./reports'));
    }

    singlePageGen(rootTemplates + '/index.handlebars', reportList);



    // Copy assets in the reports folder.
  fs.copy(rootTemplates + '/assets', reports + '/assets', function (err) {
    if (err) return console.error(err);
    console.log('success!')
  });

    console.log(rootTemplates + '/assets');
    console.log(reports + '/assets');


    if (tasksooj[global.process.argv[2]].watch.active) {
      gulp.watch(need.path.join(__dirname, '../../gulp/results/*.json'), ['builder']);
      console.log('Builder watch is running');

    }


  });

};

module.exports = builder;



