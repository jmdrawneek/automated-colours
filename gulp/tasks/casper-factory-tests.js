/**
 * CasperJS Visual diff testing.
 */

const spawn = require('child_process').spawn,
  fsextra = require('fs-extra');


module.exports = function (task, gulp, sitesettings, need) {
  'use strict';

  gulp.task('casper-factory-tests', ['builder'], function () {

    // Point to casperjs binary file, this seems to fix some odd issues with casper trying to use native node modules.
    var binpath = need.path.join(__dirname,'../../node_modules/casperjs/bin/casperjs');

    // Create array with test file names.
    var testfilepaths = [];
    fsextra.readdir(need.path.join(__dirname,'../factory-tests/'), function(err, items) {
      for (var i = 0; i < items.length; i++) {
        if (items[i].endsWith('.js')) {
          testfilepaths.push(need.path.join(__dirname,'../factory-tests/' + items[i]));
        }
      }


      var casperChild = spawn(binpath, ['test'].concat(testfilepaths));

      casperChild.stdout.on('data', function (data) {
        console.log(data.toString().slice(0, -1));
      });

      casperChild.on('close', function (code) {
        var success = code === 0; // Will be 1 in the event of failure
        // Do something with success here
      });

    });


  });
};