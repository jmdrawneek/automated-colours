var kssgen = function (task, gulp, sitesettings, need) {
  'use strict';

  var dir = sitesettings.location['styleguide'],
      templatelocation = need.path.join(__dirname, '../../' + dir + '/template'),
      csslocation = need.path.join(__dirname, '../../' + sitesettings.location['cssdest'] + '/');

  var options = {
    source: csslocation,
    destination: 'styleguide',
    css: sitesettings.location['cssdest'] + '/' + sitesettings.location['cssoutput'],
    verbose: true,
    templateDirectory: templatelocation
  };


  gulp.task('kssgen', ['sassdev'], function (cb) {
    need.kss(options);
    cb();
  });

};

module.exports = kssgen;