var sassdev = function (task, gulp, sitesettings, need, taskObj) {
  'use strict';

  var location = sitesettings.location,
      shouldstream = true;


  var sassOptions = {
    includePaths: need.sourcepaths,
    precision: 3,
    outputStyle: 'expanded'
  };

  gulp.task('sassdev', function () {
    return gulp.src(location['stylessrc'] + '/' + location['csssource'])
        .pipe(need.sourcemaps.init())
        .pipe(need.sassGlob())
        .pipe(need.changed(location['cssdest']))
        .pipe(need.sass(sassOptions))
        .pipe(need.rename(location['cssoutput']))
        .pipe(gulp.dest(location['cssdest']))
        .pipe(need.gulpif(shouldstream, need.bs.stream()))
        .on('end', function () {
          console.log(need.colors.inverse.green('----------- DEV sass files changed -----------'));
        });
  });
};

module.exports = sassdev;