(function () {
  'use strict';

  const gulp = require('gulp');
  const need = require('./gulp/gulp_dependencies');
  const sitesettings = require('./gulp/gulp_settings');

  
  const TaskTemplate = {
    subtasks: [],
    pretasks: [],
    watch: {
      active: false,
      files: [sitesettings.watch.sass]
    },
    taskData: {
      proxy: sitesettings.siteurl
    },
    browserSync: false,
  };



// Tasks
  const tasks = {
    'go': {
      subtasks: ['sassdev'],
      pretasks: ['kssgen', 'browsersync-test-area', 'browsersync-report', 'browsersync'],
      supportingtasks: ['kssgen', 'casper-factory-tests', 'builder'],
      watch: {
        active: true,
        files: [sitesettings.watch.sass]
      },
      browserSync: true,
      callbackFn: function () {

      },
      taskData: {
        index: "styleguide/section-1.html",
        serverFiles: ['output-styles/master.css', "styleguide/*.html", "js/*.js", 'images/**/*{.svg,.png}'],
        routes: {
          "/": "./styleguide"
        },
        port: 3001
      }
    },
    'setup': {
      subtasks: ['sassdev'],
      pretasks: ['casper-factory-tests', 'builder'],
      supportingtasks: ['kssgen'],
      watch: {
        active: false,
        files: []
      },
      browserSync: false,
      callbackFn: function () {
        console.log(need.colors.inverse.green('Now you can run "Gulp go".'));
      },
      taskData: {
        index: "styleguide/section-1.html",
        serverFiles: ['output-styles/master.css', "styleguide/*.html", "js/*.js", 'images/**/*{.svg,.png}'],
        routes: {
          "/": "./styleguide"
        },
        port: 3001
      }
    }
  };


  var tasksooj = {};

  // Rebuild the task object using the template as a base for each item.
  (function(tasksooj, TaskTemplate) {

    Object.keys(tasks).forEach(function(taskname) {
      var newTaskObj = Object.create(TaskTemplate);
      tasksooj[taskname] = Object.assign(newTaskObj, tasks[taskname]);
    });

  })(tasksooj, TaskTemplate);


  function genTasks(masterTaskObj, taskstype) {

    masterTaskObj[taskstype].forEach(function (task) {
      if (typeof task === 'object') {
        masterTaskObj[taskstype].forEach(function (task) {
          generateTask(task)
        })
      }
      else {
        generateTask(task)
      }
    });
    function generateTask(task) {
      getTask(task, gulp, sitesettings, need, tasksooj);
    }
  }

  // This function pulls the tasks in from their separate files and passes the configuration.
  function getTask(task, gulp, sitesettings, need, tasksooj) {

    if (task !== '' && typeof task === 'string') {
      let livetask = require('./gulp/tasks/' + task);
      livetask(task, gulp, sitesettings, need, tasksooj);
    }

  }


  // Only get the task needed, this is gathered from the global object. Arg 2 should always be the task name
  // passed from the original execution.
  var masterTaskName = global.process.argv[2];
  var masterTaskObj = tasks[masterTaskName];


  if (typeof masterTaskName === 'undefined') {
    masterTaskName = 'start';
  }

  // Generate both sets of tasks ready for use.
  genTasks(masterTaskObj, 'supportingtasks');
  genTasks(masterTaskObj, 'pretasks');
  genTasks(masterTaskObj, 'subtasks');



  gulp.task(masterTaskName, masterTaskObj.pretasks, function () {

    if (masterTaskObj.watch && masterTaskObj.watch.active) {
      gulp.watch([masterTaskObj.watch.files], masterTaskObj.subtasks);
    }
    if (masterTaskObj.browserSync) {
      need.bs.reload;
    }

    if (typeof masterTaskObj.callbackFn() == 'function') {
      masterTaskObj.callbackFn();
    }

  });


}());
